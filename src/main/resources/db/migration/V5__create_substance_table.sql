CREATE TABLE yourfarm.substance (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    description VARCHAR(50) NULL,
    name VARCHAR(50) NOT NULL,
    price DECIMAL(9,2) NOT NULL,
    amount DECIMAL(6,3) NOT NULL,
    action_id INT NOT NULL
);

ALTER TABLE yourfarm.substance
    ADD CONSTRAINT FK_SUBSTANCE_ACTION_ID FOREIGN KEY (action_id) REFERENCES yourfarm.action (id)
        ON DELETE NO ACTION ON UPDATE NO ACTION;

