### STAGE 1: Build ###
FROM gradle:6.8-jdk11 AS build

USER root

RUN mkdir -p /workspace

COPY --chown=gradle:gradle . /workspace

#USER gradle

WORKDIR /workspace

#COPY ../../. .

RUN gradle build


### STAGE 1: Run app ###
FROM openjdk:11

EXPOSE 8080

RUN mkdir /app

COPY --from=build /workspace/build/libs/*.jar /app/yourfarm-app.jar

WORKDIR /app

CMD ["java", "-jar", "yourfarm-app.jar"]
